exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    query MyQuery {
      allBrands: allContentfulInventoryBrand(filter: {node_locale: {eq: "en-CA"}}) {
        edges {
          node {
            inventory_item {
              name
              slug
              images {
                fixed(width: 100) {
                  height
                  src
                  width
                }
              }
            }
            slug
            name
            logo {
              fixed {src}
            }
          }
        }
      }
      allContentfulInventoryItem {
        edges {
          node {
            images {
              fixed(width: 640) {
                width
                height
                src
              }
            }
            inventoryNumber
            listPrice
            livePrice
            name
            price
            slug
            vendorSku
            descriptionMarkdown {
              descriptionMarkdown
            }
            node_locale
          }
        }
      }
    }
  `);
  if (result.errors) {
    throw result.errors;
  }

  // Brand detail pages
  for (brand of result.data.allBrands.edges) {
    createPage({
      path: `en/brands/${brand.node.slug}`,
      component: require.resolve('./src/templates/brand-details.js'),
      context: { brand: brand.node }
    });
  }

  // Product detail pages
  for (item of result.data.allContentfulInventoryItem.edges) {
    lang = item.node.node_locale === 'en-CA' ? 'en' : 'fr';
    urlName = item.node.node_locale === 'en-CA' ? 'products' : 'produits';
    createPage({
      path: `${lang}/${urlName}/${item.node.slug}`,
      component: require.resolve('./src/templates/item-details.js'),
      context: { item: item.node },
    });
  }
}